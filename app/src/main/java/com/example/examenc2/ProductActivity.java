package com.example.examenc2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.examenc2.database.AProductos;
import com.example.examenc2.database.Producto;

public class ProductActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private Button btnBuscar;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rbgPerecedero;
    private Button btnBorrar;
    private Button btnEditar;
    private Button btnCerrar;
    private AProductos db;
    private Producto saveObject = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        txtCodigo = findViewById(R.id.txtCodigo);
        btnBuscar = findViewById(R.id.btnBuscar);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rbgPerecedero = findViewById(R.id.rbgPerecedero);

        btnBorrar = findViewById(R.id.btnBorrar);
        btnEditar = findViewById(R.id.btnEditar);
        btnCerrar = findViewById(R.id.btnCerrar);

        db = new AProductos(this);

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.openDatabase();
                db.deleteProduct(Long.parseLong(getText(txtCodigo)));
                Toast.makeText(ProductActivity.this, "Se borro el registro", Toast.LENGTH_SHORT).show();
                db.close();
                limpiar();

            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.openDatabase();
                Producto producto = db.getProducto(Long.parseLong(getText(txtCodigo)));
                if(producto == null)
                {
                    Toast.makeText(ProductActivity.this, "No se encontro", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    saveObject = producto;
                    txtMarca.setText(saveObject.getMarca());
                    txtNombre.setText(saveObject.getNombreProducto());
                    txtPrecio.setText(String.valueOf(saveObject.getPrecio()));
                    rbgPerecedero.check(saveObject.isPerecedero() ? R.id.rbPerecedero : R.id.rbNoPerecedero);
                }
                db.close();

            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.openDatabase();
                if(validar())
                {
                    Producto producto = new Producto();
                    producto.setCodigo(Integer.parseInt(getText(txtCodigo)));
                    producto.setId(saveObject.getId());
                    producto.setNombreProducto(getText(txtNombre));
                    producto.setPrecio(Float.parseFloat(getText(txtPrecio)));
                    producto.setMarca(getText(txtMarca));
                    producto.setPerecedero(rbgPerecedero.getCheckedRadioButtonId() == R.id.rbPerecedero);
                    if (db.updateProduct(producto) != -1)
                        Toast.makeText(ProductActivity.this, "Se actualizo correctamente", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(ProductActivity.this, "Se produjo un error", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(ProductActivity.this, "Llene todos los campos", Toast.LENGTH_SHORT).show();

                }
                limpiar();
                db.close();

            }
        });

    }
    private void limpiar()
    {
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        saveObject = null;

    }
    private boolean validar(){
        if(validarCampo(txtCodigo) || validarCampo(txtNombre) ||
                validarCampo(txtPrecio) || validarCampo(txtMarca))
            return false;
        else
            return true;
    }

    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }


}
