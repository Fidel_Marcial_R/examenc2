package com.example.examenc2.database;

public class Producto {
    private long id;
    private String nombreProducto;
    private String marca;
    private float precio;
    private boolean perecedero;
    private int codigo;

    public Producto() {
    }

    public Producto(long id, String nombreProducto, String marca, float precio, boolean perecedero, int codigo) {
        this.id = id;
        this.nombreProducto = nombreProducto;
        this.marca = marca;
        this.precio = precio;
        this.perecedero = perecedero;
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float preacio) {
        this.precio = preacio;
    }

    public boolean isPerecedero() {
        return perecedero;
    }

    public void setPerecedero(boolean perecedero) {
        this.perecedero = perecedero;
    }
}
